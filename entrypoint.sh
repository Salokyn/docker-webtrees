#!/bin/bash
# Author Nicolas Gif
# Greatly inspired from https://github.com/nextcloud/docker/blob/master/17.0/fpm/entrypoint.sh

# Read env variables from docker secrets
[ -f "$MYSQL_PASSWORD_FILE" ]    && MYSQL_PASSWORD="$(cat "$MYSQL_PASSWORD_FILE")"
[ -f "$MYSQL_DATABASE_FILE" ]    && MYSQL_DATABASE="$(cat "$MYSQL_DATABASE_FILE")"
[ -f "$MYSQL_USER_FILE" ]        && MYSQL_USER="$(cat "$MYSQL_USER_FILE")"
[ -f "$WT_ADMIN_NAME_FILE" ]     && WT_ADMIN_NAME="$(cat "$WT_ADMIN_NAME_FILE")"
[ -f "$WT_ADMIN_USER_FILE" ]     && WT_ADMIN_USER="$(cat "$WT_ADMIN_USER_FILE")"
[ -f "$WT_ADMIN_PASSWORD_FILE" ] && WT_ADMIN_PASSWORD="$(cat "$WT_ADMIN_PASSWORD_FILE")"
[ -f "$WT_ADMIN_EMAIL_FILE" ]    && WT_ADMIN_EMAIL="$(cat "$WT_ADMIN_EMAIL_FILE")"

set -ex

# version_greater A B returns whether A > B
version_greater() {
  [ "$(printf '%s\n' "$@" | sort -t '.' -n -k1,1 -k2,2 -k3,3 | head -n 1)" != "$1" ]
}

installed_version="0.0.0"
if [ -f /var/www/html/app/Webtrees.php ]; then
  installed_version="$(php -r 'require("/var/www/html/app/Webtrees.php"); echo Fisharebest\Webtrees\Webtrees::VERSION;')"
fi

image_version="$(php -r 'require("/usr/src/webtrees/app/Webtrees.php"); echo Fisharebest\Webtrees\Webtrees::VERSION;')"

if version_greater "$installed_version" "$image_version"; then
  echo "Installed version ($installed_version) is greater than image version ($image_version)" >&2
  exit 1
fi

if version_greater "$image_version" "$installed_version"; then

  if [ "$(id -u)" = 0 ]; then
    rsync_options="-rlDog --chown www-data:root"
  else
    rsync_options="-rlD"
  fi

# shellcheck disable=SC2086
  rsync $rsync_options --delete --exclude "/data/" /usr/src/webtrees/ /var/www/html/
  
  if [ "$installed_version" = "0.0.0" ];then

    CONF=/var/www/html/data/config.ini.php

    # shellcheck disable=SC2153
    if [ -n "$MYSQL_DATABASE" ] && \
       [ -n "$MYSQL_USER" ] && \
       [ -n "$MYSQL_PASSWORD" ] && \
       [ -n "$MYSQL_HOST" ]
    then

      echo "; <?php exit; ?> DO NOT DELETE THIS LINE" > $CONF

      if [ -z "$MYSQL_PORT" ]; then
        MYSQL_PORT=3306
      fi
      if [ -z "$MYSQL_PREFIX" ]; then
        MYSQL_PREFIX=wt_
      fi

      echo "dbtype=\"mysql\"
dbhost=\"$MYSQL_HOST\"
dbport=\"$MYSQL_PORT\"
dbuser=\"$MYSQL_USER\"
dbpass=\"$MYSQL_PASSWORD\"
dbname=\"$MYSQL_DATABASE\"
tblpfx=\"$MYSQL_PREFIX\"" >> $CONF

      if [ -n "$BASE_URL" ]; then
        echo "base_url=\"$BASE_URL\"" >> $CONF
      fi

      if [ -n "$WT_ADMIN_USER" ] && \
         [ -n "$WT_ADMIN_PASSWORD" ] && \
         [ -n "$WT_ADMIN_EMAIL" ]
      then

        if [ -z "$WT_ADMIN_NAME" ]; then
          WT_ADMIN_NAME=$WT_ADMIN_USER
        fi

        max_retries=20
        try=0
        # Update admin
        echo "Update admin user..."
        PHPCOMMAND="\$pdo = new PDO('mysql:host=$MYSQL_HOST;dbname=$MYSQL_DATABASE', '$MYSQL_USER', '$MYSQL_PASSWORD'); \$request='UPDATE ${MYSQL_PREFIX}user SET user_name=\"$WT_ADMIN_USER\", email=\"$WT_ADMIN_EMAIL\", real_name=\"$WT_ADMIN_NAME\", password=\"'.crypt('$WT_ADMIN_PASSWORD','').'\" WHERE user_id=1'; echo 'request : '.\$request; exit(\$pdo->query(\$request)->rowCount()-1 === 0);"
        until php -r "$PHPCOMMAND" || [ "$try" -gt "$max_retries" ]
        do
          echo "retrying..."
          try=$((try+1))
          sleep 10s
        done
        if [ "$try" -gt "$max_retries" ]; then
          echo "updating of admin user failed!"
          exit 1
        fi
      fi
    fi
  fi
fi
chown -R www-data:root /var/www/html/data

exec "$@"
