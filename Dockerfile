FROM php:8-fpm-alpine

ARG VERSION=2.1.15

RUN apk add --no-cache rsync bash
# install the PHP extensions we need
RUN set -ex; \
    \
    apk add --no-cache --virtual .build-deps \
        freetype-dev \
        icu-dev \
        libjpeg-turbo-dev \
        libwebp-dev \
        libzip-dev \
    ; \
    \
    docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp; \
    docker-php-ext-install -j $(nproc) \
        gd \
        intl \
        opcache \
        pdo_mysql \
        zip \
        exif \
    ; \
    \
    runDeps="$( \
        scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )"; \
    apk add --virtual .webtrees-phpext-rundeps $runDeps; \
    apk del .build-deps

# set recommended PHP.ini settings
RUN echo 'memory_limit=256M' > /usr/local/etc/php/conf.d/memory-limit.ini; \
    printf 'upload_max_filesize=10M\npost_max_size=10M\n' > /usr/local/etc/php/conf.d/upload-limit.ini; \
    \
    mkdir /var/www/data; \
    chown -R www-data:root /var/www; \
    chmod -R g=u /var/www

VOLUME ["/var/www/html","/var/www/html/data"]

RUN set -ex; \
    \
    apk add --no-cache --virtual .fetch-deps unzip; \
    curl -fsSL "https://github.com/fisharebest/webtrees/releases/download/$VERSION/webtrees-$VERSION.zip" -o "webtrees.zip"; \
    unzip -q "webtrees.zip" -d /usr/src/; \
    rm -f "webtrees.zip"; \
    apk del .fetch-deps
   
COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["php-fpm"]
